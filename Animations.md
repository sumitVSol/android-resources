**Overview - Get a complete overview on Animaimation:**

- [Animation Intro](https://android-developers.googleblog.com/2011/02/animation-in-honeycomb.html)
- [Advanced](https://android-developers.googleblog.com/2011/05/introducing-viewpropertyanimator.html)
  
**Google Materials:**

- [PROPERTY ANIMATOR](https://developer.android.com/guide/topics/graphics/prop-animation.html)
- [ValueAnimator class](https://developer.android.com/reference/android/animation/ValueAnimator.html)
- [ObjectAnimator class](https://developer.android.com/reference/android/animation/ObjectAnimator.html)
- [ValuePropertyAnimator class](https://developer.android.com/reference/android/view/ViewPropertyAnimator.html)

**Blog for performance and graphics:**

- [Graphic blog](http://graphics-geek.blogspot.in/)

**Vector drawable and animators:**

- [Understanding vector drawable commands](https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands)
- [Dynamically changing vector drawables](https://github.com/harjot-oberai/VectorMaster)
- [Icon Animation Techniques](https://www.androiddesignpatterns.com/2016/11/introduction-to-icon-animation-techniques.html#conclusion-putting-it-all-together)